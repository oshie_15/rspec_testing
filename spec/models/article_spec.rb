# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Article, type: :model do
  it 'test a number to be positive' do
    expect(1).to be_positive
    expect(5).to be > 3
  end
  it 'tests article object' do
    # article = Article.create({title:"Sample artilce", content:"Sample content", slug:"Sample slug"})
    article = FactoryBot.create(:article) # we can remove FactoryBot keyword, it can work because we already configured it inside rails helper
    expect(article.title).to eq('Sample article')
  end
  # Check validation
  describe '#validatios' do
    let(:article) { build(:article) }
    it 'test that factory is valid' do
      expect(article).to be_valid # same as article.valid? == true
    end
    it 'has an invalid title' do
      article.title = ''
      expect(article).not_to be_valid
      expect(article.errors[:title]).to include("can't be blank")
    end
    it 'has an invalid content' do
      article.content = ''
      expect(article).not_to be_valid
      expect(article.errors[:content]).to include("can't be blank")
    end
    it 'has an invalid slug' do
      article.slug = ''
      expect(article).not_to be_valid
      expect(article.errors[:slug]).to include("can't be blank")
    end
  end
end
